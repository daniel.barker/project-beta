from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=100, unique=True, null=True)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveBigIntegerField()

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.CharField(max_length=12)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salespeople",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )
    price = models.IntegerField()

    def __str__(self):
        return f"{self.automobile}"

    def get_api_url(self):
        return reverse("api_price", kwargs={"pk": self.id})
