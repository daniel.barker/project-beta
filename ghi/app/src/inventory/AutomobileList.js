import React, {useEffect, useState} from "react"
import { NavLink } from "react-router-dom";

function AutomobileList({ automobiles, getAutomobiles}) {
  const deleteAutomobile = async (id) => {
    fetch(`http://localhost:8090/api/automobiles/${id}/`, {
      method:"delete",
    })
    .then(() => {
      return getAutomobiles()
    })
  }
  if (automobiles === undefined) {
    return null
  }
  return (
  <>
    <table className="table table-success table-striped table-hover">
      <thead>
        <tr>
          <th>Color</th>
          <th>Year</th>
          <th>VIN</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map((automobile) => {
          return (
            <tr key={automobile.id}>
              <td>{automobile.color}</td>
              <td>{automobile.year}</td>
              <td>{automobile.vin}</td>
              <td>{automobile.sold}</td>
              <td>
                  <button type="button" value={automobile.id} onClick={() => deleteAutomobile(automobile.id)}>Delete Automobile</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}
export default AutomobileList;
