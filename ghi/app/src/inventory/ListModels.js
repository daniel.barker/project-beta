import React, {useEffect, useState } from "react";

function ListModels() {
    const [models, setModels] = useState([]);

    useEffect(() => {
        const fetchModels = async () => {
            try {
                const response = await fetch ('http://localhost:8100/api/models/');
                if (!response.ok) {
                    throw new Error('Failed to fetch models');
                }
                const data = await response.json();
                setModels(data.models);
            } catch (error) {
                console.error(error);
            }
        };
        fetchModels();
    }, []);

    return (
        <div>
            <h2>Models</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Model Name</th>
                        <th>Model Manufacturer</th>
                        <th>Model Picture</th>
                        <th>Model ID</th>

                    </tr>
                </thead>
                <tbody>
                    {models.map(model => (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td><img width="200" src={model.picture_url} /></td>
                            <td>{model.id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );

}

export default ListModels
