import React, {useEffect, useState } from "react";

function ListInventory() {
    const [inventory, setInventory] = useState([]);

    useEffect(() => {
        const fetchInventory = async () => {
            try {
                const response = await fetch ('http://localhost:8100/api/automobiles/');
                if (!response.ok) {
                    throw new Error('Failed to fetch inventory');
                }
                const data = await response.json();
                setInventory(data.autos);
            } catch (error) {
                console.error(error);
            }
        };
        fetchInventory();
    }, []);

    return (
        <div>
            <h2>Cars in Inventory</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {inventory.map(car => (
                        <tr key={car.id}>
                            <td>{car.vin}</td>
                            <td>{car.color}</td>
                            <td>{car.year}</td>
                            <td>{car.model.manufacturer.name}</td>
                            <td>{car.model.name}</td>
                            <td>{car.sold ? "Sold" : "In Stock" }</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );

}

export default ListInventory
